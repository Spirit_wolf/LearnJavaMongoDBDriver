package com.gzc.mrg;

import com.gzc.enums.CollectionNameEnum;
import com.gzc.enums.MongoDBType;
import com.gzc.pojo.*;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

/**
 * @author Spirit_wolf
 * @date 2018/10/20
 */
public class GameDbMrgTest {
    @Mock
    MongoClient mongoClient;
    @Mock
    CodecRegistry codecRegistry;
    @Mock
    EnumMap<MongoDBType, MongoDatabase> dbMap;

    @InjectMocks
    GameDbMrg gameDbMrg;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateNeedIndex() throws Exception {
        gameDbMrg.createNeedIndex();
    }

    @Test
    public void testGetPlayerFromDB() throws Exception {
        Player result = gameDbMrg.getPlayerFromDB("360", 1540115169840L);
        Player qqPlayer = createPlayer("360", 1540115169840L, "名字1540115169840");

        Assert.assertEquals(qqPlayer.getPlatPid(), result.getPlatPid());
        Assert.assertEquals(qqPlayer.getAccountGuid(), result.getAccountGuid());
    }

    private Player createPlayer(String platName, long accountGuid, String name) {
        //--------------物品----------------------
        Goods goods1 = new Goods(UUID.randomUUID().toString().hashCode(), 100, 48);
        Goods goods2 = new Goods(UUID.randomUUID().toString().hashCode(), 101, 11);
        Goods goods3 = new Goods(UUID.randomUUID().toString().hashCode(), 102, 123);

        BagItemInfo bagItemInfo = new BagItemInfo();
        bagItemInfo.getGoodsGuid2goods().put(goods1.getGoodsGuid() + "", goods1);
        bagItemInfo.getGoodsGuid2goods().put(goods2.getGoodsGuid() + "", goods2);
        bagItemInfo.getGoodsGuid2goods().put(goods3.getGoodsGuid() + "", goods3);

        //--------------装备----------------------
        Goods equip1 = new Goods(UUID.randomUUID().toString().hashCode(), 200, 123);
        Goods equip2 = new Goods(UUID.randomUUID().toString().hashCode(), 201, 312);
        Goods equip3 = new Goods(UUID.randomUUID().toString().hashCode(), 202, 2221);
        BagItemInfo equipBag = new BagItemInfo();
        equipBag.getGoodsGuid2goods().put(equip1.getGoodsGuid() + "", equip1);
        equipBag.getGoodsGuid2goods().put(equip2.getGoodsGuid() + "", equip2);
        equipBag.getGoodsGuid2goods().put(equip3.getGoodsGuid() + "", equip3);

        ArrayList<Address> addresses = new ArrayList<>();
        Address address1 = new Address("street111","city111","zip111");
        Address address2 = new Address("street222","city222","zip333");
        Address address3 = new Address("street333","city333","zip333");
        addresses.add(address1);
        addresses.add(address2);
        addresses.add(address3);


        BagInfo bagInfo = new BagInfo();
        bagInfo.getBagType2bagItem().put(BagType.Goods.name(), bagItemInfo);
        bagInfo.getBagType2bagItem().put(BagType.Equip.name(), equipBag);

        Player player = new Player(System.currentTimeMillis(),
                platName,
                accountGuid,
                name,
                Math.abs(UUID.randomUUID().toString().hashCode()),
                Math.abs(UUID.randomUUID().toString().hashCode()),
                bagInfo,
                addresses);

        return player;
    }

    @Test
    public void testGetPlayer() throws Exception {
        Player result = gameDbMrg.getPlayer(0L, "name");
        Assert.assertEquals(new Player(0L, "platPid", 0L, "name", (short) 0, 0L, new BagInfo(), Arrays.<Address>asList(new Address("street", "city", "zip"))), result);
    }

    @Test
    public void testGetCollection() throws Exception {
        MongoCollection<Player> result = gameDbMrg.getCollection(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.PLAYER, Player.class);
        Assert.assertEquals(null, result);
    }

    @Test
    public void testCreateIndex() throws Exception {
//        gameDbMrg.createIndex(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, "key");
    }

    @Test
    public void testInsertDocument() throws Exception {
        Player player = createPlayer("QQ", System.currentTimeMillis(), "java" + System.currentTimeMillis());
        gameDbMrg.insertDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.PLAYER, player);
    }

    @Test
    public void testInsertDocument2() throws Exception {
        Player player = createPlayer("YY", System.currentTimeMillis(), "Netty" + System.currentTimeMillis());
        gameDbMrg.insertDocument(gameDbMrg.getDataBase(MongoDBType.MONGODB_TYPE_SERVER), CollectionNameEnum.PLAYER, player);
    }

//    @Test
//    public void testFindDocument() throws Exception {
//        List<T> result = gameDbMrg.findDocument(null, "collectionName", null, null);
//        Assert.assertEquals(Arrays.<T>asList(new T()), result);
//    }
//
//    @Test
//    public void testFindDocuments() throws Exception {
//        List<T> result = gameDbMrg.findDocuments(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, null);
//        Assert.assertEquals(Arrays.<T>asList(new T()), result);
//    }
//
//    @Test
//    public void testFindDocuments2() throws Exception {
//        List<T> result = gameDbMrg.findDocuments(MongoDBType.MONGODB_TYPE_SERVER, "collectionName", null, null);
//        Assert.assertEquals(Arrays.<T>asList(new T()), result);
//    }
//
//    @Test
//    public void testFindFirstDocument() throws Exception {
//        T result = gameDbMrg.findFirstDocument(null, CollectionNameEnum.ACCOUNT_INFO, null, null);
//        Assert.assertEquals(new T(), result);
//    }
//
//    @Test
//    public void testFindFirstDocument2() throws Exception {
//        T result = gameDbMrg.findFirstDocument(MongoDBType.MONGODB_TYPE_SERVER, "collectionName", null, null);
//        Assert.assertEquals(new T(), result);
//    }
//
//    @Test
//    public void testFindFirstDocument3() throws Exception {
//        T result = gameDbMrg.findFirstDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, null);
//        Assert.assertEquals(new T(), result);
//    }
//
//    @Test
//    public void testFindFirstDocument4() throws Exception {
//        T result = gameDbMrg.findFirstDocument(null, "collectionName", null, null);
//        Assert.assertEquals(new T(), result);
//    }

    @Test
    public void testFindDocumentsWithoutObjectId() throws Exception {
        List<Document> result = gameDbMrg.findDocumentsWithoutObjectId(MongoDBType.MONGODB_TYPE_SERVER, "collectionName", null, Arrays.<String>asList("String"));
        Assert.assertEquals(Arrays.<Document>asList(null), result);
    }

    @Test
    public void testFindFirstDocument5() throws Exception {
        Document result = gameDbMrg.findFirstDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, Arrays.<String>asList("String"));
        Assert.assertEquals(null, result);
    }

    @Test
    public void testFindFirstDocument6() throws Exception {
        Document result = gameDbMrg.findFirstDocument(MongoDBType.MONGODB_TYPE_SERVER, "collectionName", null, Arrays.<String>asList("String"));
        Assert.assertEquals(null, result);
    }

//    @Test
//    public void testReplaceDocument() throws Exception {
//        UpdateResult result = gameDbMrg.replaceDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, new T());
//        Assert.assertEquals(null, result);
//    }

//    @Test
//    public void testReplaceDocumentWithInsertIfAbsent() throws Exception {
//        UpdateResult result = gameDbMrg.replaceDocumentWithInsertIfAbsent(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, new T());
//        Assert.assertEquals(null, result);
//    }

//    @Test
//    public void testFindDocumentByPage() throws Exception {
//        List<T> result = gameDbMrg.findDocumentByPage(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, null, 0, 0);
//        Assert.assertEquals(Arrays.<T>asList(new T()), result);
//    }

//    @Test
//    public void testFindDocumentsWithLimit() throws Exception {
//        List<T> result = gameDbMrg.findDocumentsWithLimit(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, null, 0);
//        Assert.assertEquals(Arrays.<T>asList(new T()), result);
//    }

    @Test
    public void testCountCollection() throws Exception {
        long result = gameDbMrg.countCollection(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO);
        Assert.assertEquals(0L, result);
    }

    @Test
    public void testCountCollection2() throws Exception {
        long result = gameDbMrg.countCollection(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null);
        Assert.assertEquals(0L, result);
    }

    @Test
    public void testDeleteOneDocument() throws Exception {
        gameDbMrg.deleteOneDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null);
    }

    @Test
    public void testDeleteManyDocument() throws Exception {
        gameDbMrg.deleteManyDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null);
    }

    @Test
    public void testUpdateOneDocument() throws Exception {
        gameDbMrg.updateOneDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, null);
    }

    @Test
    public void testUpdateOneDocument2() throws Exception {
        gameDbMrg.updateOneDocument(MongoDBType.MONGODB_TYPE_SERVER, "collName", null, null);
    }

    @Test
    public void testUpdateManyDocument() throws Exception {
        gameDbMrg.updateManyDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, null);
    }

    @Test
    public void testUpsertOneDocument() throws Exception {
        gameDbMrg.upsertOneDocument(MongoDBType.MONGODB_TYPE_SERVER, "collectionName", null, null);
    }

    @Test
    public void testDropCollection() throws Exception {
        gameDbMrg.dropCollection(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO);
    }

    @Test
    public void testDropCollection2() throws Exception {
        gameDbMrg.dropCollection(MongoDBType.MONGODB_TYPE_SERVER, "collectionName");
    }

    @Test
    public void testFindMaxOrMin() throws Exception {
        Document result = gameDbMrg.findMaxOrMin(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null);
        Assert.assertEquals(null, result);
    }

//    @Test
//    public void testFindCursor() throws Exception {
//        Iterator<T> result = gameDbMrg.findCursor(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, null);
//        Assert.assertEquals(null, result);
//    }

//    @Test
//    public void testFindCursor2() throws Exception {
//        Iterator<T> result = gameDbMrg.findCursor(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, null, Arrays.<String>asList("String"), null);
//        Assert.assertEquals(null, result);
//    }

//    @Test
//    public void testFindCursor3() throws Exception {
//        Iterator<T> result = gameDbMrg.findCursor(null, CollectionNameEnum.ACCOUNT_INFO, null, null);
//        Assert.assertEquals(null, result);
//    }

    @Test
    public void testCheckExist() throws Exception {
        boolean result = gameDbMrg.checkExist(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.ACCOUNT_INFO, "documentKeyName", "value");
        Assert.assertEquals(true, result);
    }

    @Test
    public void testRenameCollection() throws Exception {
        boolean result = gameDbMrg.renameCollection(MongoDBType.MONGODB_TYPE_SERVER, "collectionName", "newName");
        Assert.assertEquals(true, result);
    }

//    @Test
//    public void testSerializable() throws Exception {
//        byte[] result = gameDbMrg.serializable(new T());
//        Assert.assertArrayEquals(new byte[]{(byte) 0}, result);
//    }

//    @Test
//    public void testDeserializable() throws Exception {
//        T result = gameDbMrg.deserializable(null, null);
//        Assert.assertEquals(new T(), result);
//    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme