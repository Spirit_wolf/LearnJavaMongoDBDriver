package com.gzc.codec;

import com.gzc.pojo.Goods;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

/**
 * Created by Spirit_wolf on 2018/10/12.
 */
public class GoodsCodec implements Codec<Goods> {
    @Override
    public Goods decode(BsonReader reader, DecoderContext decoderContext) {
        reader.readStartDocument();

        long guid = reader.readInt64("goodsGuid");
        int goodsId = reader.readInt32("goodsId");
        int num = reader.readInt32("goodsNum");

        reader.readEndDocument();

        return new Goods(guid, goodsId, num);
    }

    @Override
    public void encode(BsonWriter writer, Goods value, EncoderContext encoderContext) {
        writer.writeStartDocument();

        writer.writeInt64("goodsGuid", value.getGoodsGuid());
        writer.writeInt32("goodsId", value.getGoodsId());
        writer.writeInt32("goodsNum", value.getNum());

        writer.writeEndDocument();
    }

    @Override
    public Class<Goods> getEncoderClass() {
        return Goods.class;
    }
}
