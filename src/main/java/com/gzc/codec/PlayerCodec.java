package com.gzc.codec;

import com.gzc.enums.DocumentKeyNameEnum;
import com.gzc.pojo.Address;
import com.gzc.pojo.BagInfo;
import com.gzc.pojo.Player;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistry;

/**
 * Created by Spirit_wolf on 2018/10/12.
 */
public class PlayerCodec implements Codec<Player> {
    private final CodecRegistry codecRegistry;

    public PlayerCodec(CodecRegistry codecRegistry) {
        this.codecRegistry = codecRegistry;
    }

    @Override
    public Player decode(BsonReader reader, DecoderContext decoderContext) {
        Player player = new Player();

        reader.readStartDocument();
        player.setObjectId(reader.readObjectId());
        player.setSessionId(reader.readInt64("sessionId"));
        player.setPlatPid(reader.readString("platPid"));
        player.setAccountGuid(reader.readInt64("accountGuid"));
        player.setName(reader.readString("name"));
        player.setLevel(reader.readInt32("level"));
        player.setExp(reader.readInt64("exp"));

        //背包信息
        reader.readName(DocumentKeyNameEnum.bagInfo.name());
        codecRegistry.get(BagInfo.class).decode(reader, decoderContext);
        //地址信息
        {
            final int addressSize = reader.readInt32("addressSize");
            reader.readStartArray();
            for (int i = 0; i < addressSize; i++) {
                reader.readStartDocument();
                final String street = reader.readString("street");
                final String city = reader.readString("city");
                final String zip = reader.readString("zip");
                reader.readEndDocument();

                Address address = new Address(street, city, zip);
                player.getAddresses().add(address);
            }
            reader.readEndArray();
        }
        reader.readEndDocument();
        return player;
    }

    @Override
    public void encode(BsonWriter writer, Player value, EncoderContext encoderContext) {
        writer.writeStartDocument();
        writer.writeInt64("sessionId", value.getSessionId());
        writer.writeString("platPid", value.getPlatPid());
        writer.writeInt64("accountGuid", value.getAccountGuid());
        writer.writeString("name", value.getName());
        writer.writeInt32("level", value.getLevel());
        writer.writeInt64("exp", value.getExp());

        //背包信息
        writer.writeName(DocumentKeyNameEnum.bagInfo.name());
        codecRegistry.get(BagInfo.class).encode(writer, value.getBagInfo(), encoderContext);
        //地址信息
        {
            writer.writeInt32("addressSize", value.getAddresses().size());
            writer.writeStartArray("addresses");
            for (Address address : value.getAddresses()) {
                writer.writeStartDocument();

                writer.writeString("street", address.getStreet());
                writer.writeString("city", address.getCity());
                writer.writeString("zip", address.getZip());
                writer.writeEndDocument();
            }
            writer.writeEndArray();
        }
        writer.writeEndDocument();
    }

    @Override
    public Class<Player> getEncoderClass() {
        return Player.class;
    }
}
