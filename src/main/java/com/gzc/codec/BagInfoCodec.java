package com.gzc.codec;

import com.gzc.pojo.BagInfo;
import com.gzc.pojo.BagItemInfo;
import com.gzc.pojo.Goods;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistry;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by Spirit_wolf on 2018/10/12.
 */
public class BagInfoCodec implements Codec<BagInfo> {
    private CodecRegistry registry;

    public BagInfoCodec(CodecRegistry registry) {
        this.registry = registry;
    }

    @Override
    public BagInfo decode(BsonReader reader, DecoderContext decoderContext) {
        BagInfo bagInfo = new BagInfo();

        reader.readStartDocument();

        final int allBagMapSize = reader.readInt32("allBagMapSize");
        reader.readStartArray();
        for (int i = 0; i < allBagMapSize; i++) {
            reader.readStartDocument();
            final String type = reader.readString("type");
            BagItemInfo bagItemInfo = bagInfo.getBagType2bagItem().get(type);
            if (null == bagItemInfo) {
                bagItemInfo = new BagItemInfo();
                bagInfo.getBagType2bagItem().put(type, bagItemInfo);
            }

            //某个类型的背包
            {
                final int bagMapSize = reader.readInt32("bagMapSize");
                reader.readStartArray();
                for (int j = 0; j < bagMapSize; j++) {
                    //序列化单个物品
                    Goods goods = registry.get(Goods.class).decode(reader, decoderContext);

                    bagItemInfo.getGoodsGuid2goods().put(goods.getGoodsGuid() + "", goods);
                }
                reader.readEndArray();
            }
            reader.readEndDocument();
        }
        reader.readEndArray();

        reader.readEndDocument();

        return bagInfo;
    }

    @Override
    public void encode(BsonWriter writer, BagInfo value, EncoderContext encoderContext) {
        writer.writeStartDocument();

        writer.writeInt32("allBagMapSize", value.getBagType2bagItem().size());
        writer.writeStartArray("allBagMap");
        for (Iterator<Map.Entry<String, BagItemInfo>> it = value.getBagType2bagItem().entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, BagItemInfo> entry = it.next();
            String type = entry.getKey();
            BagItemInfo bagItemInfo = entry.getValue();

            writer.writeStartDocument();
            writer.writeString("type", type);
            //某个类型的背包
            {
                writer.writeInt32("bagMapSize", bagItemInfo.getGoodsGuid2goods().size());
                writer.writeStartArray("bagMap");
                for (Iterator<Map.Entry<String, Goods>> itt = bagItemInfo.getGoodsGuid2goods().entrySet().iterator(); itt.hasNext(); ) {
                    Map.Entry<String, Goods> entryBag = itt.next();
                    Goods goods = entryBag.getValue();

                    //序列化单个物品
                    registry.get(Goods.class).encode(writer, goods, encoderContext);
                }
                writer.writeEndArray();
            }
            writer.writeEndDocument();
        }
        writer.writeEndArray();

        writer.writeEndDocument();
    }

    @Override
    public Class<BagInfo> getEncoderClass() {
        return BagInfo.class;
    }
}
