package com.gzc.enums;

/**
 * Created by Spirit_wolf on 2018/10/12.
 */
public enum DocumentKeyNameEnum {
    /**
     * mongodb对象自带的GUUID
     */
    _id,
    /**
     * 账户ID
     */
    userId,
    /**
     * 背包信息
     */
    bagInfo,
}
