package com.gzc.enums;

/**
 * Created by Spirit_wolf on 2018/10/12.
 */
public enum CollectionNameEnum {
    /**
     * 账号信息集合
     */
    ACCOUNT_INFO,
    /**
     * 数据库版本号集合
     */
    DB_VERSION_SET,
    /**
     * 玩家数据集合
     */
    PLAYER,
}
