package com.gzc.mrg;

import com.gzc.enums.CollectionNameEnum;
import com.gzc.enums.DocumentKeyNameEnum;
import com.gzc.enums.MongoDBType;
import com.gzc.pojo.Player;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Spirit_wolf
 * @date 2018/10/20
 */
public class GameDbMrg extends BaseMongoMrg {

    private final MongoClient mongoClient;

    private static final String DB_NAME = "inDriver3_8_2";

    public GameDbMrg() {
        ServerAddress mongoAddress1 = new ServerAddress("localhost", 27111);
        ServerAddress mongoAddress2 = new ServerAddress("localhost", 27112);
        ServerAddress mongoAddress3 = new ServerAddress("localhost", 27113);
        List<ServerAddress> addressList = new ArrayList<>();
        addressList.add(mongoAddress1);
        addressList.add(mongoAddress2);
        addressList.add(mongoAddress3);

        this.mongoClient = createMongoClient(addressList, "", "", DB_NAME);

        MongoDatabase serverDataBase = mongoClient.getDatabase(DB_NAME);
        dbMap.put(MongoDBType.MONGODB_TYPE_SERVER, serverDataBase);
    }

    @Override
    public void createNeedIndex() {

    }

    /**
     * 通过平台ID和账号ID查询角色数据
     */
    public Player getPlayerFromDB(String platPid, long accountGuid) {
        Bson bson = Filters.eq("platPid", platPid);
        Bson bson2 = Filters.eq("accountGuid", accountGuid);
        //通过平台ID和账号ID来查询玩家数据
        Bson bson3 = Filters.and(bson, bson2);

        return findFirstDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.PLAYER, bson3, Player.class);
    }

    public Player getPlayer(long accountGuid, String name) {
        Bson bson = Filters.eq(DocumentKeyNameEnum.userId.name(), accountGuid);
        Bson bson2 = Filters.eq("name", name);
        //通过账户ID和游戏角色名字来查询玩家数据
        Bson bson3 = Filters.and(bson, bson2);

        return findFirstDocument(MongoDBType.MONGODB_TYPE_SERVER, CollectionNameEnum.PLAYER, bson3, Player.class);
    }

}
