package com.gzc.pojo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Spirit_wolf on 2018/9/30.
 * 单个类型背包项信息
 */
public class BagItemInfo {
    /**
     * key:物品GUID，V:物品
     */
//    private Map<Long, Goods> goodsGuid2goods = new HashMap<>();
    private Map<String, Goods> goodsGuid2goods = new HashMap<>();

    public BagItemInfo() {
    }

    public Map<String, Goods> getGoodsGuid2goods() {
        return goodsGuid2goods;
    }

    public void setGoodsGuid2goods(Map<String, Goods> goodsGuid2goods) {
        this.goodsGuid2goods = goodsGuid2goods;
    }
}
