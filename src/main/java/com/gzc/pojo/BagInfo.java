package com.gzc.pojo;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Spirit_wolf on 2018/9/30.
 * 所有类型的背包信息
 */
public class BagInfo {
//    private Map<BagType, BagItemInfo> bagType2bagItem = new EnumMap<>(BagType.class);
    private Map<String, BagItemInfo> bagType2bagItem = new HashMap<>();

    public BagInfo() {
    }

    public Map<String, BagItemInfo> getBagType2bagItem() {
        return bagType2bagItem;
    }

    public void setBagType2bagItem(Map<String, BagItemInfo> bagType2bagItem) {
        this.bagType2bagItem = bagType2bagItem;
    }
}

