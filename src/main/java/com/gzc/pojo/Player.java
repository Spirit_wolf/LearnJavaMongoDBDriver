package com.gzc.pojo;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Spirit_wolf on 2018/9/30.
 */
public class Player {
    private ObjectId objectId = null;
    private long sessionId;
    /**
     * 平台ID
     */
    private String platPid;
    /**
     * 账号ID
     */
    private long accountGuid;
    /**
     * 角色名字
     */
    private String name;
    /**
     * 角色等级
     */
    private int level;
    /**
     * 经验值
     */
    private long exp;
    /**
     * 背包信息
     */
    private BagInfo bagInfo = new BagInfo();

    private List<Address> addresses = new ArrayList<>();

    public Player() {
    }

    public Player(long sessionId, String platPid, long accountGuid, String name, int level, long exp, BagInfo bagInfo,
                  List<Address> addresses) {
        this.sessionId = sessionId;
        this.platPid = platPid;
        this.accountGuid = accountGuid;
        this.name = name;
        this.level = level;
        this.exp = exp;
        this.bagInfo = bagInfo;
        this.addresses = addresses;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public String getPlatPid() {
        return platPid;
    }

    public void setPlatPid(String platPid) {
        this.platPid = platPid;
    }

    public long getAccountGuid() {
        return accountGuid;
    }

    public void setAccountGuid(long accountGuid) {
        this.accountGuid = accountGuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }

    public BagInfo getBagInfo() {
        return bagInfo;
    }

    public void setBagInfo(BagInfo bagInfo) {
        this.bagInfo = bagInfo;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
