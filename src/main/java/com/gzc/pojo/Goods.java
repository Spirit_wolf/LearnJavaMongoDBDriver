package com.gzc.pojo;

/**
 * Created by Spirit_wolf on 2018/9/30.
 * 物品对象
 */
public class Goods {
    private long goodsGuid;
    private int goodsId;
    private int num;

    public Goods() {
    }

    public Goods(long goodsGuid, int goodsId, int num) {
        this.goodsGuid = goodsGuid;
        this.goodsId = goodsId;
        this.num = num;
    }

    public long getGoodsGuid() {
        return goodsGuid;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public int getNum() {
        return num;
    }
}
