package com.gzc.pojo;

/**
 * 背包类型
 */
public enum BagType {
    Equip,
    Goods,
}
